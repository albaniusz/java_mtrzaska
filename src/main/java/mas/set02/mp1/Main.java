package mas.set02.mp1;

import mas.set02.mp1.logic.Company;
import mas.set02.mp1.logic.Employee;
import mas.set02.mp1.logic.Truck;
import org.hibernate.Session;

import java.sql.Driver;
import java.util.Date;

/**
 * Main
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("Maven + Hibernate + Postgresql");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		if (true) { // INIT

			Company company =  new Company();
			company.setName("Kompania");
			company.setTown("Warszawa");
			session.save(company);


			Truck truck1 = new Truck();
			truck1.setCompany(company);
			truck1.setRegisterNumber("WA1234");
			session.save(truck1);

			Employee employee1=new Employee();
			employee1.setCompany(company);
			employee1.setSurname("Adam");
			employee1.setLastName("Adamski");
			employee1.setPesel("12341209876");
			employee1.setDateOfEmployment(new Date());
			session.save(employee1);

			employee1.setTruck(truck1);
			session.save(employee1);
			truck1.setDriver(employee1);
			session.save(truck1);

//			Truck truck2 = new Truck();
//			truck2.setCompany(company);
//			truck2.setRegisterNumber("WA1235");

//			Truck truck3 = new Truck();
//			truck3.setCompany(company);
//			truck3.setRegisterNumber("WA1235");


		}




		System.out.println();

		session.getTransaction().commit();
	}
}
