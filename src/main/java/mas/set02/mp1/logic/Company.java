package mas.set02.mp1.logic;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Company
 */
@Entity
@Table(name = "company", schema = "public")
public class Company {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name", unique = true, nullable = false, length = 32)
	private String name;

	@Column(name = "town", unique = true, nullable = false, length = 32)
	private String town;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	private Set<Employee> employees = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	private Set<Truck> trucks = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public Set<Truck> getTrucks() {
		return trucks;
	}

	public void setTrucks(Set<Truck> trucks) {
		this.trucks = trucks;
	}
}
