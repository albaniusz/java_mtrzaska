package mas.set02.mp1.logic;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Employee
 */
@Entity
@Table(name = "employee", schema = "public")
public class Employee {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "surname", unique = false, nullable = false, length = 32)
	private String surname;

	@Column(name = "second_name", unique = false, nullable = true, length = 32)
	private String secondName;

	@Column(name = "lastname", unique = false, nullable = false, length = 32)
	private String lastName;

	@Column(name = "pesel", unique = true, nullable = false, length = 11)
	private String pesel;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_employment", unique = false, nullable = false)
	private Date dateOfEmployment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", nullable = false)
	private Company company;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "driver")
	private Truck truck;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public Date getDateOfEmployment() {
		return dateOfEmployment;
	}

	public void setDateOfEmployment(Date dateOfEmployment) {
		this.dateOfEmployment = dateOfEmployment;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}
}
