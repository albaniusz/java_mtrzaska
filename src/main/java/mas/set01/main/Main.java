package mas.set01.main;

import mas.set01.main.logic.Company;
import mas.set01.main.logic.Extension;
import mas.set01.main.logic.Storage;
import mas.set01.main.logic.machine.Machine;

/**
 * Main
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			Company company;
			company = new Company("CiuCiu");

			if (Extension.isStorageExists()) {
				System.out.println("odczyt ekstensji");
				Extension.read();
			} else {
				System.out.println("tworzenie ekstensji");

				// dodanie magazynu
				Storage storage = new Storage("Magazyn nr.1");
				company.setStorage(storage);

				// dodanie maszyn
				Machine machine1 = new Machine("MDC001");
				company.addMachine(machine1);

				Machine machine2 = new Machine("MDC002");
				company.addMachine(machine2);

				// dodanie skladnikow

				// dodanie polew

				// dodanie nadzienia

				// dodnie produktow

				// dodanie zamowienia

				// utworzenie produkcji


				Extension.write();
			}

			System.out.println(company);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
