package mas.set01.main.logic;

import java.io.Serializable;

/**
 * Storage
 */
public class Storage extends Extension implements Serializable {

	private String name;
	private Company company;

	/**
	 * @param name
	 */
	public Storage(String name) {
		super(Storage.class);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCompany(Company company) {

		if (this.company != company) {
			this.company = company;
			company.setStorage(this);
		}
	}
}
