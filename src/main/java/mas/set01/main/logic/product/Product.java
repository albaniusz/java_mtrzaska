package mas.set01.main.logic.product;

import java.util.Vector;

/**
 * Product
 */
public abstract class Product {

	protected Filling filling;
	protected Coating coating;
	protected String name;
	protected Vector<Component> components;

	/**
	 * Product constructor
	 *
	 * @param name Name of product
	 */
	public Product(String name) {
		this.name = name;
	}

	public void addComponent(Component component) {
		components.add(component);
	}

	public Filling getFilling() {
		return filling;
	}

	public Vector<Component> getComponents() {
		return components;
	}

	public String getName() {
		return name;
	}

	public Coating getCoating() {
		return coating;
	}
}
