package mas.set01.main.logic;

import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Extension
 */
public class Extension {

	private static String storageFile = "./resources/storage.dat";
	private static Hashtable<String, List> storage = new Hashtable<String, List>();

	/**
	 * @param className
	 */
	public Extension(String className) {

		if (storage.containsKey(className)) {
			storage.get(className).add(this);
		} else {
			ArrayList<Object> list = new ArrayList<Object>();
			list.add(this);
			storage.put(className, list);
		}
	}

	/**
	 * @param storageClass
	 */
	public Extension(Class<Storage> storageClass) {
		//To change body of created methods use File | Settings | File Templates.
	}

	/**
	 * @return
	 */
	public static boolean isStorageExists() {

		File f = new File(storageFile);
		return f.exists() ? true : false;
	}

	/**
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void read() throws IOException, ClassNotFoundException {

		ObjectInputStream in = new ObjectInputStream(new FileInputStream(storageFile));
		storage = (Hashtable) in.readObject();
		in.close();
	}

	/**
	 * @throws IOException
	 */
	public static void write() throws IOException {

		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(storageFile));
		out.writeObject(storage);
		out.close();
	}
}
