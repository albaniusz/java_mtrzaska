package mas.set01.main.logic;

import mas.set01.main.logic.machine.Machine;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Company
 */
public class Company extends Extension implements Serializable {

	private String name;
	private Storage storage;
	private HashMap<String, Machine> machines = new HashMap<String, Machine>();

	/**
	 * @param name
	 */
	public Company(String name) {
		super(name);
		this.name = name;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {

		if (this.storage != storage) {
			this.storage = storage;
			storage.setCompany(this);
		}
	}

	public String getName() {
		return name;
	}

	public void addMachine(Machine machine) {

		if (!machines.containsKey(machine.getName())) {
			machines.put(machine.getName(), machine);
			machine.setCompany(this);
		}
	}

	@Override
	public String toString() {
		return "Company{" +
				"name='" + name + '\'' +
				", storage=" + storage +
				", machines=" + machines +
				'}';
	}
}
