package mas.set01.main.logic.machine;

import mas.set01.main.logic.Company;

import java.io.Serializable;

/**
 * Machine
 */
public class Machine implements Serializable {

	private String name;
	private Company company;

	/**
	 * @param name
	 */
	public Machine(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {

		if (this.company != company) {
			this.company = company;
			company.addMachine(this);
		}
	}
}
