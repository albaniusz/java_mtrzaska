package mas.set01.mp3.doit.customer;

/**
 * RegularCustomer
 */
public class RegularCustomer extends Customer {

	/**
	 * @param name
	 * @param nip
	 * @param regon
	 */
	public RegularCustomer(String name, String nip, String regon) {
		super(name, nip, regon);
	}

	/**
	 * @param predecessor
	 */
	public RegularCustomer(Customer predecessor) {
		super(predecessor.getName(), predecessor.getNip(), predecessor.getRegon());
	}

	@Override
	public int getResponseTime() {
		return 10;
	}
}
