package mas.set01.mp3.doit.customer;

import mas.set01.mp3.doit.Company;

/**
 * Customer
 */
public abstract class Customer {

	protected Company company;
	protected String name;
	protected String nip;
	protected String regon;

	/**
	 * @param name
	 * @param nip
	 * @param regon
	 */
	public Customer(String name, String nip, String regon) {
		this.name = name;
		this.nip = nip;
		this.regon = regon;
	}

	/**
	 * @param predecessor
	 * @param name
	 * @param nip
	 * @param regon
	 */
	public Customer(Customer predecessor, String name, String nip, String regon) {
		this(name, nip, regon);
	}

	public abstract int getResponseTime();

	public String getName() {
		return name;
	}

	public String getNip() {
		return nip;
	}

	public String getRegon() {
		return regon;
	}

	@Override
	public String toString() {
		return "Klient:" + name + " " + nip + "/" + regon + "; reakcja: " + getResponseTime();
	}

	public void setCompany(Company company) {
		if (this.company != company) {
			this.company = company;
			company.addCustomer(this);
		}
	}
}
