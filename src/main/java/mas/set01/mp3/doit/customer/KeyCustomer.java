package mas.set01.mp3.doit.customer;

/**
 * KeyCustomer
 */
public class KeyCustomer extends Customer {

	/**
	 * @param name
	 * @param nip
	 * @param regon
	 */
	public KeyCustomer(String name, String nip, String regon) {
		super(name, nip, regon);
	}

	/**
	 * @param predecessor
	 */
	public KeyCustomer(Customer predecessor) {
		super(predecessor.getName(), predecessor.getNip(), predecessor.getRegon());
	}

	@Override
	public int getResponseTime() {
		return 5;
	}
}
