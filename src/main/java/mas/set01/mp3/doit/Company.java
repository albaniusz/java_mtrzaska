package mas.set01.mp3.doit;

import mas.set01.mp3.doit.customer.Customer;
import mas.set01.mp3.doit.employee.Employee;

import java.util.Vector;

/**
 * Company
 */
public class Company {

	private String name;
	private Vector<Employee> employees = new Vector<Employee>();
	private Vector<Customer> customers = new Vector<Customer>();

	/**
	 * @param name
	 */
	public Company(String name) {
		this.name = name;
	}

	/**
	 * @param employee
	 */
	public void addEmployee(Employee employee) {

		if (!employees.contains(employee)) {
			employees.add(employee);
			employee.setCompany(this);
		}
	}

	/**
	 * @param customer
	 */
	public void addCustomer(Customer customer) {

		if (!customers.contains(customer)) {
			customers.add(customer);
			customer.setCompany(this);
		}
	}
}
