package mas.set01.mp3.doit.employee;

/**
 * Male
 */
public class Male extends Gender {

	/**
	 * @return
	 */
	@Override
	public int calculateParentalLeave() {
		return 5;
	}
}
