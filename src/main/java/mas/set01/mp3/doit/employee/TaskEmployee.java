package mas.set01.mp3.doit.employee;

/**
 * TaskEmployee
 */
public class TaskEmployee extends Employee {

	private boolean whetherABill = false;

	/**
	 * @param gender
	 * @param name
	 */
	protected TaskEmployee(Gender gender, String name) {
		super(gender, name);
	}

	/**
	 * @return
	 */
	@Override
	public double calculateSalary() {
		return getBaseSalary() * (1.0 - 0.09);
	}

	/**
	 * Czy wystawil rachunek na zlecenie
	 *
	 * @return
	 */
	public boolean isWhetherABill() {
		return whetherABill;
	}
}
