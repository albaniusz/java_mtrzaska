package mas.set01.mp3.doit.employee;

/**
 * FullTimeEmployee
 */
public class FullTimeEmployee extends Employee {

	private int usedVacation = 0;
	private int limitVacation = 20;

	/**
	 * @param gender
	 * @param name
	 * @throws Exception
	 */
	public FullTimeEmployee(Gender gender, String name) throws Exception {
		super(gender, name);
	}

	/**
	 * @return
	 */
	@Override
	public double calculateSalary() {
		return getBaseSalary() * (1.0 - 0.18);
	}

	/**
	 * Zwraca ilosc dni wykorzystanego urlopu
	 *
	 * @return
	 */
	public int getUsedVacation() {
		return usedVacation;
	}

	/**
	 * Zwraca ilosc dni dostepnego urlopu
	 *
	 * @return
	 */
	public int getAvalibleVacation() {
		return limitVacation - usedVacation;
	}
}
