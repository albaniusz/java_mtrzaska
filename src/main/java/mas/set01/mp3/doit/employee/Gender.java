package mas.set01.mp3.doit.employee;

/**
 * Gender
 */
public abstract class Gender {

    private Employee employee;

    /**
     * Obliczenie wymiaru urlopu rodzicielskiego
     *
     * @return ilosc dni urlopu rodzicielskiego
     */
    public abstract int calculateParentalLeave();

    /**
     * Ustawia pracownika
     *
     * @param employee
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
