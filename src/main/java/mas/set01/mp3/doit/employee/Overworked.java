package mas.set01.mp3.doit.employee;

/**
 * Overworked
 */
public class Overworked extends FullTimeEmployee implements ITaskEmployee {

	private TaskEmployee taskEmployee;

	/**
	 * @param gender
	 * @param name
	 * @throws Exception
	 */
	public Overworked(Gender gender, String name) throws Exception {
		super(gender, name);
		this.taskEmployee = (TaskEmployee) new Employee(Employee.TYPE_TASK, gender, name);
	}

	/**
	 * @return
	 */
	@Override
	public double calculateSalary() {
		return super.calculateSalary() + taskEmployee.calculateSalary();
	}

	/**
	 * @return
	 */
	@Override
	public boolean isWhetherABill() {
		return taskEmployee.isWhetherABill();
	}
}
