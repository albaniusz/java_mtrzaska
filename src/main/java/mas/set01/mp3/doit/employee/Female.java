package mas.set01.mp3.doit.employee;

/**
 * Female
 */
public class Female extends Gender {

	/**
	 * @return
	 */
	@Override
	public int calculateParentalLeave() {
		return 10;
	}
}
