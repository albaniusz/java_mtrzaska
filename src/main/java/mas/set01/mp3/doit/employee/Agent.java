package mas.set01.mp3.doit.employee;

/**
 * Agent
 */
public class Agent extends Employee {

	private String nip;
	private String regon;

	/**
	 * @param gender
	 * @param name
	 */
	protected Agent(Gender gender, String name) {
		super(gender, name);
	}

	@Override
	public double calculateSalary() {
		return getBaseSalary() * 1.23;
	}

	public String getNip() {
		return nip;
	}

	public String getRegon() {
		return regon;
	}
}
