package mas.set01.mp3.doit.employee;

import mas.set01.mp3.doit.Company;

import java.util.EnumSet;

enum EmployeeType {
	Employee,
	Programmer,
	Tester
};

/**
 * Employee
 */
public class Employee {

	public static final int TYPE_FULLTIME = 1;
	public static final int TYPE_TASK = 2;
	public static final int TYPE_AGENT = 3;
	public static final int TYPE_OVERWORKED = 4;
	protected Gender gender;
	protected Company company;
	protected String name;
	private Employee employee;
	protected double baseSalary = 3500.00;

	private EnumSet<EmployeeType> employeeType = EnumSet.<EmployeeType>of(EmployeeType.Employee);

	/**
	 * @param gender
	 * @param name
	 */
	protected Employee(Gender gender, String name) {

		this.gender = gender;
		gender.setEmployee(this);

		this.name = name;
	}

	/**
	 * @param type
	 * @param gender
	 * @param name
	 * @throws Exception
	 */
	public Employee(int type, Gender gender, String name) throws Exception {
		switch (type) {
			case TYPE_FULLTIME:
				employee = new FullTimeEmployee(gender, name);
				break;
			case TYPE_TASK:
				employee = new TaskEmployee(gender, name);
				break;
			case TYPE_AGENT:
				employee = new Agent(gender, name);
				break;
			case TYPE_OVERWORKED:
				employee = new Overworked(gender, name);
				break;
			default:
				throw new Exception("Nieobslugiwany typ klasy pracownika");
		}
	}

	public void setCompany(Company company) {
		if (this.company != company) {
			this.company = company;
			company.addEmployee(this);
		}
	}

	/**
	 * @return
	 */
	public double calculateSalary() {
		return employee.calculateSalary();
	}

	public double getBaseSalary() {
		return baseSalary;
	}
}
