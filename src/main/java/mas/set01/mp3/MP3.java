package mas.set01.mp3;

import mas.set01.mp3.doit.Company;
import mas.set01.mp3.doit.customer.Customer;
import mas.set01.mp3.doit.customer.KeyCustomer;
import mas.set01.mp3.doit.customer.RegularCustomer;
import mas.set01.mp3.doit.employee.Employee;
import mas.set01.mp3.doit.employee.Female;
import mas.set01.mp3.doit.employee.Male;

/**
 * MP3
 */
public class MP3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// firma doIT
			Company company = new Company("doIT");

			// pracownicy
			// etatowy
			Employee employee1 = new Employee(Employee.TYPE_FULLTIME, new Male(), "Adam Bąk");
			// na zlecenie
			Employee employee2 = new Employee(Employee.TYPE_TASK, new Female(), "Beata Cicha");
			// na dzialalnosc
			Employee employee3 = new Employee(Employee.TYPE_AGENT, new Male(), "Cezary Dąb");
			// pracoholik
			Employee employee4 = new Employee(Employee.TYPE_OVERWORKED, new Male(), "Dionizy Ewenement");

			// klient (zwykly)
			Customer customer = new RegularCustomer("Schabex", "111-222-33-11", "123456");
			System.out.println(customer);

			// zmiana statusu klienta na kluczowu
			customer = new KeyCustomer(customer);
			System.out.println(customer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
