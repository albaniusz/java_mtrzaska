package mas.set01.mp2;

import mas.set01.mp2.transport.*;

/**
 * MP2
 */
public class MP2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// firma
			Company company = new Company("Speedex");

			// pracownicy
			company.addEmployee(new Employee("Adam", "Bąk", "75090223611"));

			// pojazdy i wiazanie pracownikow z pojazdami
			Vehicle vehicle1 = new Vehicle("WX 1234");
			company.addVehicle(vehicle1);

			vehicle1.setDriver(company.findEmployee("75090223611"));

			// utworzenie listu przewozowego i dodanie uslugi transportowej
			Transport transport1 = new Transport();
			Waybill.createWaybill(transport1, "Pomidory dla Keczup C.O.");
			transport1.setExecutor(company, vehicle1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
