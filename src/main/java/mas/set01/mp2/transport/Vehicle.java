package mas.set01.mp2.transport;

import java.util.Vector;

/**
 * Vehicle
 *
 * Pojazd
 */
public class Vehicle {

	private Company company;
	private Employee driver;
	private Vector<Transport> transports = new Vector<Transport>();
	private String registrationNumber;

	/**
	 * @param registrationNumber
	 */
	public Vehicle(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	/**
	 * @param transport
	 */
	public void addTransport(Transport transport) {
		if (!transports.contains(transport)) {
			transports.add(transport);
			transport.setVehicle(this);
		}
	}

	/**
	 * Przydziela kierowce do pojazdu
	 *
	 * @param driver
	 */
	public void setDriver(Employee driver) {

		if (this.driver != driver) {
			this.driver = driver;
			driver.setVehicle(this);
		}
	}

	/**
	 * Przydziela pojazd do firmy
	 *
	 * @param company
	 */
	public void setCompany(Company company) {

		if (this.company != company) {
			this.company = company;
			company.addVehicle(this);
		}
	}
}
