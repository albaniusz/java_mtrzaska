package mas.set01.mp2.transport;

/**
 * Employee
 *
 * Pracownik
 */
public class Employee {

	private Company company;
	private Vehicle vehicle;
	private String firstname;
	private String surname;
	private String pesel;

	/**
	 * @param firstname
	 * @param surname
	 * @param pesel
	 */
	public Employee(String firstname, String surname, String pesel) {
		this.firstname = firstname;
		this.surname = surname;
		this.pesel = pesel;
	}

	public String getPesel() {
		return pesel;
	}

	/**
	 * Ustawia firme
	 *
	 * @param company
	 */
	public void setCompany(Company company) {

		if (this.company != company) {
			this.company = company;
			company.addEmployee(this);
		}
	}

	/**
	 * Przydziela pojazd do kierowcy
	 *
	 * @param vehicle
	 */
	public void setVehicle(Vehicle vehicle) {

		if (this.vehicle != vehicle) {
			this.vehicle = vehicle;
			vehicle.setDriver(this);
		}
	}
}
