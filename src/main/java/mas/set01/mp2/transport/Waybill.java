package mas.set01.mp2.transport;

/**
 * Waybill
 *
 * List przewozowy
 */
public class Waybill {

	private Transport transport;
	private String name;

	/**
	 * @param name
	 */
	private Waybill(String name) {
		this.name = name;
	}

	/**
	 * @param transport
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Waybill createWaybill(Transport transport, String name) throws Exception {
		if (transport == null) {
			throw new Exception("Transport nie istnieje");
		}

		Waybill waybill = new Waybill(name);
		transport.setWaybill(waybill);

		return waybill;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}
}
