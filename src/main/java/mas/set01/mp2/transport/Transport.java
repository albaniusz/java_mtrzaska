package mas.set01.mp2.transport;

/**
 * Transport
 *
 * Usluga transportowa
 */
public class Transport {

	private Waybill waybill;
	private Company company;
	private Vehicle vehicle;

	/**
	 * @param company
	 */
	public void setCompany(Company company) {

		if (this.company != company) {
			this.company = company;
			company.addTransport(this);
		}
	}

	public void setVehicle(Vehicle vehicle) {
		if (this.vehicle != vehicle) {
			this.vehicle = vehicle;
			vehicle.addTransport(this);
		}
	}

	public void setWaybill(Waybill waybill) {
		if (this.waybill != waybill) {
			this.waybill = waybill;
			waybill.setTransport(this);
		}
	}

	/**
	 * Ustawia wykonawce zlecenia
	 *
	 * @param company firma obslugujaca zlecenie
	 * @param vehicle pojazd wykonujacy zlecenie
	 */
	public void setExecutor(Company company, Vehicle vehicle) {
		setCompany(company);
		setVehicle(vehicle);
	}
}
