package mas.set01.mp2.transport;

import java.util.HashMap;
import java.util.Vector;

/**
 * Company
 *
 * Przedsiebiorstwo
 */
public class Company {
	private Vector<Vehicle> vehicles = new Vector<Vehicle>();
	private HashMap<String, Employee> employees = new HashMap<String, Employee>();
	private Vector<Transport> transports = new Vector<Transport>();
	private String name;

	/**
	 * @param name
	 */
	public Company(String name) {
		this.name = name;
	}

	/**
	 * @param employee
	 */
	public void addEmployee(Employee employee) {
		if (!employees.containsKey(employee.getPesel())) {
			employees.put(employee.getPesel(), employee);
			employee.setCompany(this);
		}
	}

	/**
	 * @param transport
	 */
	public void addTransport(Transport transport) {
		if (!transports.contains(transport)) {
			transports.add(transport);
			transport.setCompany(this);
		}
	}

	/**
	 * @param vehicle
	 */
	public void addVehicle(Vehicle vehicle) {
		if (!vehicles.contains(vehicle)) {
			vehicles.add(vehicle);
			vehicle.setCompany(this);
		}
	}

	/**
	 * Wyszukuje pracownika po numerze PESEL
	 *
	 * @param pesel
	 * @return
	 * @throws Exception
	 */
	public Employee findEmployee(String pesel) throws Exception {
		if (!employees.containsKey(pesel)) {
			throw new Exception("Nie ma pracownika z numerem PESEL:" + pesel);
		}

		return employees.get(pesel);
	}
}
